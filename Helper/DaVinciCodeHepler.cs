﻿using System;
using System.Collections.Generic;
using DaVinciCode.Enum;

namespace DaVinciCode.Helper
{
    public class DaVinciCodeHepler
    {
        private int _min;

        private int _max;

        private int _maximumLimit;

        private int _boomNumber;

        private List<int> _upperAndLowerLimits = new List<int>();

        private static DaVinciCodeHepler _instance = null;

        public static DaVinciCodeHepler Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DaVinciCodeHepler();
                }
                return _instance;
            }
        }

        public void BoomNumberInit()
        {
            var random = new Random(Guid.NewGuid().GetHashCode());
            _min = 0;
            _max = _maximumLimit;
            _boomNumber = random.Next(_min, _max);
            _upperAndLowerLimits = new List<int> {
                _min, _max
            };
        }

        public DaVinciCodeResult Guess(int guessNumber)
        {
            var result = DaVinciCodeResult.None;

            if ((guessNumber >= _max || guessNumber <= _min) && (!_upperAndLowerLimits.Contains(guessNumber)))
            {
                result = DaVinciCodeResult.ErrorRange;
            }
            else if (guessNumber == _boomNumber)
            {
                result = DaVinciCodeResult.Boom;
            }
            else
            {
                _min = guessNumber < _boomNumber ? guessNumber : _min;
                _max = guessNumber > _boomNumber ? guessNumber : _max;
                if (guessNumber < _boomNumber)
                {
                    _upperAndLowerLimits.RemoveAll(limit => limit <= guessNumber);
                }
                else if (guessNumber > _boomNumber)
                {
                    _upperAndLowerLimits.RemoveAll(limit => limit >= guessNumber);
                }
            }
            return result;
        }

        public string GetRange()
        {
            return $"{_min}~{_max}";
        }

        public void SetMaximumLimit(int maximumLimit)
        {
            _maximumLimit = maximumLimit;
        }
    }
}