﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DaVinciCode.Helper;
using DaVinciCode.Model;
using DaVinciCode.Enum;

namespace DaVinciCode
{
    public partial class Main : Form
    {
        private List<Player> _players = null;

        public Main()
        {
            InitializeComponent();
            DaVinciCodeHepler.Instance.SetMaximumLimit(999);
            PlayersInit();
            btnSendNumber.Enabled = false;
            btnStart.Enabled = true;
        }

        private void PlayersInit()
        {
            _players = new List<Player>();
            for (int i = 1; i <= 3; i++)
            {
                _players.Add(new Player()
                {
                    Name = $"玩家{i}"
                });
            }
            lblPlayerName.Text = _players.First().Name;
            DaVinciCodeHepler.Instance.BoomNumberInit();
        }

        private void btnGuessNumber_Click(object sender, EventArgs e)
        {
            var currentPlayer = lblPlayerName.Text;

            var guessNumber = Convert.ToInt32(txtGuessNumber.Text);
            var result = DaVinciCodeHepler.Instance.Guess(guessNumber);

            Player removePlayer = null;

            if (result == DaVinciCodeResult.Boom)
            {
                txtMessage.Text = $"{currentPlayer}您輸了!{Environment.NewLine}{txtMessage.Text}";
                removePlayer = _players.Where(p => p.Name == currentPlayer).FirstOrDefault();
                DaVinciCodeHepler.Instance.BoomNumberInit();
            }
            else if (result == DaVinciCodeResult.ErrorRange)
            {
                txtMessage.Text = $"請輸入合法範圍!{Environment.NewLine}{txtMessage.Text}";
                return;
            }

            lblPlayerName.Text = _players.SkipWhile(p => p.Name != currentPlayer).Skip(1).FirstOrDefault()?.Name;
            if (removePlayer != null)
            {
                _players.Remove(removePlayer);
            }

            if (lblPlayerName.Text == string.Empty)
            {
                lblPlayerName.Text = _players.FirstOrDefault().Name;
            }

            if (_players.Count == 1)
            {
                txtMessage.Text = $"{lblPlayerName.Text}獲勝!{Environment.NewLine}{txtMessage.Text}";
                btnSendNumber.Enabled = false;
                btnStart.Enabled = true;
                btnStart.Text = "重新開始";
            }
            else
            {
                txtMessage.Text = $"範圍:{DaVinciCodeHepler.Instance.GetRange()}{Environment.NewLine}{txtMessage.Text}";
            }

            txtGuessNumber.Text = string.Empty;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            btnSendNumber.Enabled = true;
            btnStart.Enabled = false;
            if (btnStart.Text == "開始")
            {
                btnStart.Text = "重新開始";
            }
            else if (btnStart.Text == "重新開始")
            {
                PlayersInit();
            }
            txtMessage.Text = $"範圍:{DaVinciCodeHepler.Instance.GetRange()}{Environment.NewLine}{txtMessage.Text}";
        }
    }
}